package com.sofkaspacestation.controllers;

import com.sofkaspacestation.models.SpaceshipDTO;
import com.sofkaspacestation.services.ISpaceship;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;

import javax.validation.Valid;
import java.util.List;

/**
 * Api Rest para el consumir y realizar acciones con las naves
 */
@RestController
@RequestMapping(value = "rest/space-stations")
public class SpaceshipController {

    @Autowired
    private ISpaceship iSpaceship;

    /**
     * End-Point para guardas naves espaciales
     * @param spaceshipDTO
     */
    @PostMapping
    public void save(@Valid @RequestBody SpaceshipDTO spaceshipDTO){
        iSpaceship.save(spaceshipDTO);
    }

    /**
     * End-Point para listar todas las naves espaciales
     * @return List<?>
     */
    @GetMapping(value = "/list")
    public List<?> findAll(){
        return iSpaceship.findAll();
    }

    /**
     * End-Point para filtar las naves espaciales por tipo
     * @param spaceshipTypeId
     * @return List<?>
     */
    @GetMapping(value = "/list/type/{spaceshipTypeId}")
    public List<?> findBySpaceshipTypeId(@PathVariable Integer spaceshipTypeId){
        return iSpaceship.findBySpaceshipTypeId(spaceshipTypeId);
    }
}
