package com.sofkaspacestation.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Entidad donde se mapea la tabla tb_spaceship_type
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_spaceship_type")
public class SpaceshipType {
    @Id
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Column(name = "name", length = 200)
    private String name;

    public SpaceshipType(Integer id) {
        this.id = id;
    }
}
