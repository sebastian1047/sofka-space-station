package com.sofkaspacestation.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Entidad donde se mapea la tabla tb_spaceship
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tb_spaceship")
public class Spaceship implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private Integer id;

    @NotNull
    @Column(name = "name", length = 200)
    private String name;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER,optional = false)
    @JoinColumn(name = "spaceship_type_id")
    private SpaceshipType spaceshipType;

    @NotNull
    @Column(name = "speed", length = 200)
    private Float speed;

    @NotNull
    @Column(name = "propulsion_system", length = 200)
    private String propulsionSystem;

    @NotNull
    @Column(name = "spaceship_action", length = 200)
    private String spaceshipAction;
}
