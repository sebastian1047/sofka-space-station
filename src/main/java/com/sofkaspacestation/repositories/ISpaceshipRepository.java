package com.sofkaspacestation.repositories;

import com.sofkaspacestation.entities.Spaceship;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Interface para gestionar información de la tabla de naves espaciales
 */
@Repository
public interface ISpaceshipRepository extends CrudRepository<Spaceship, Integer> {
    /**
     * Query para filtrar las naves espaciales por tipo
     * @param spaceshipTypeId
     * @return List<Spaceship
     */
    List<Spaceship> findBySpaceshipTypeId(Integer spaceshipTypeId);
}
