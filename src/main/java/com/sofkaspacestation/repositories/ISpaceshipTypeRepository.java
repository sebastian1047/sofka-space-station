package com.sofkaspacestation.repositories;

import com.sofkaspacestation.entities.SpaceshipType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Interface para gestionar información de la tabla de tipos naves espaciales
 */
@Repository
public interface ISpaceshipTypeRepository extends CrudRepository<SpaceshipType, Integer> {
}
