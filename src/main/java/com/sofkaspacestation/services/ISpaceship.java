package com.sofkaspacestation.services;

import com.sofkaspacestation.models.SpaceshipDTO;
import com.sofkaspacestation.models.SpaceshipInfoDTO;

import java.util.List;

/**
 * Interfas para gestionar las acciones a realizar con las naves
 */
public interface ISpaceship {
    void save(SpaceshipDTO spaceshipDTO);

    List<SpaceshipInfoDTO> findAll();

    List<SpaceshipInfoDTO>  findBySpaceshipTypeId(Integer spaceshipTypeId);
}
