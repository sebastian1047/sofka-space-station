package com.sofkaspacestation.services.impl;

import com.sofkaspacestation.entities.Spaceship;
import com.sofkaspacestation.entities.SpaceshipType;
import com.sofkaspacestation.models.SpaceshipDTO;
import com.sofkaspacestation.models.SpaceshipInfoDTO;
import com.sofkaspacestation.repositories.ISpaceshipRepository;
import com.sofkaspacestation.services.ISpaceship;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Capa lógica donde se gestiona el guardado y consulta de naves
 */
@Service
public class SpaceshipImpl implements ISpaceship {
    private ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private ISpaceshipRepository iSpaceshipRepository;

    /**
     * Guarda aeronaves
     * @param spaceshipDTO
     */
    @Override
    public void save(SpaceshipDTO spaceshipDTO) {
        iSpaceshipRepository.save(new Spaceship(spaceshipDTO.getId(), spaceshipDTO.getName(), new SpaceshipType(spaceshipDTO.getSpaceshipTypeId()), spaceshipDTO.getSpeed(), spaceshipDTO.getPropulsionSystem(), getSpaceshipAction(spaceshipDTO.getSpaceshipTypeId())));
    }

    /**
     * Lista todas las naves
     * @return List<SpaceshipInfoDTO>
     */
    @Override
    public List<SpaceshipInfoDTO> findAll() {
        return modelMapper.map(iSpaceshipRepository.findAll(), new TypeToken<List<SpaceshipInfoDTO>>() {
        }.getType());
    }

    /**
     * Lista los tipos de naves por tipo
     * @param spaceshipTypeId
     * @return List<SpaceshipInfoDTO>
     */
    @Override
    public List<SpaceshipInfoDTO> findBySpaceshipTypeId(Integer spaceshipTypeId) {
        return modelMapper.map(iSpaceshipRepository.findBySpaceshipTypeId(spaceshipTypeId), new TypeToken<List<SpaceshipInfoDTO>>() {
        }.getType());
    }

    /**
     * Gestiona la acción que hace cada tipo de nave
     * @param spaceshipTypeId
     * @return String
     */
    private String getSpaceshipAction(Integer spaceshipTypeId) {
        switch (spaceshipTypeId) {
            case 1:
                return "Lanzar carga util";
            case 2:
                return "Estudiar otros cuerpos celestes";
            case 3:
                return "Enviar seres humanos al espacio";
        }
        return null;
    }
}
