package com.sofkaspacestation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase principal
 */
@SpringBootApplication
public class SofkaSpaceStationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SofkaSpaceStationApplication.class, args);
	}

}
