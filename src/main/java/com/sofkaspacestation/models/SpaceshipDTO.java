package com.sofkaspacestation.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO el cual se utiliza para enviar la info de las naves a guardar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpaceshipDTO {
    private Integer id;
    private String name;

    private Integer spaceshipTypeId;

    private Float speed;

    private String propulsionSystem;
}
