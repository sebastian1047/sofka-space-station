package com.sofkaspacestation.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO en el cual esta la info de los tipos de naves a retornar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpaceshipTypeDTO {
    private Integer id;

    private String name;
}
