package com.sofkaspacestation.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO en el cual esta la info de las naves a retornar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SpaceshipInfoDTO {
    private Integer id;

    private String name;

    private SpaceshipTypeDTO spaceshipType;

    private Float speed;

    private String propulsionSystem;

    private String spaceshipAction;
}
